extern crate tic_tac_toe;
use tic_tac_toe::game::{Game, Mark, Outcome};
use tic_tac_toe::agent::{RandomAgent, RLAgent, Agent};

use std::env;

fn play_game<T : Agent>( agent_1 : &mut T, agent_2 : &mut T, verbose: bool) -> Outcome {
    let mut current_game = Game::new();
    let mut counter = 0;

    loop{
        if counter % 2 == 0 {
            current_game.set_mark( agent_1.get_mark(), agent_1.next_move(&current_game) ).unwrap();
        } else {
            current_game.set_mark( agent_2.get_mark(), agent_2.next_move(&current_game) ).unwrap();
        };
        counter += 1;

        if verbose { println!("{} \n ------", current_game); }

        match current_game.evaluate(){
            Some(outcome) => {
                agent_1.evaluate_game(&outcome);
                agent_2.evaluate_game(&outcome);
                return outcome;
            },
            None => continue,
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let iterations = if args.len() > 1 {
            args[1].parse::<usize>().unwrap_or(10)
        } else {
            10
    };

    println!("Iterations: {:?}", iterations);

    let mut agent_1 = RLAgent::new(
        Mark::PlayerOne,
        (0.5,-1.0,0.5), //Reward Scheme
        0.0, //Intial Values
    );
    let mut agent_2 = RLAgent::new(
        Mark::PlayerTwo,
        (0.5,-1.0,0.5),
        0.0
    );

    let (mut count_one, mut count_two) = (0,0);
    for _ in 0..(10 * iterations) as usize{
        play_game(&mut agent_1, &mut agent_2, false);
    }


    for _ in 0..iterations-1 as usize{
        match play_game(&mut agent_1, &mut agent_2, false){
            Outcome::PlayerOne => count_one += 1,
            Outcome::PlayerTwo => count_two += 1,
            _ => (),
        };
    }

    match play_game(&mut agent_1, &mut agent_2, true){
        Outcome::PlayerOne => count_one += 1,
        Outcome::PlayerTwo => count_two += 1,
        _ => (),
    };

    println!("P1: {:.2}", count_one as f32 / iterations as f32 * 100.0 );
    println!("P2: {:.2}", count_two as f32 / iterations as f32 * 100.0 );
    println!("P3: {:.2}", (iterations - count_one - count_two) as f32 / iterations as f32 * 100.0 );
}
