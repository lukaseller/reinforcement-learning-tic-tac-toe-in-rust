use std::fmt;

//Other implementation would be setting only the marks

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Mark{
    PlayerOne,
    PlayerTwo,
    Empty,
}
//Whenever we use -> just use Option<Mark> -> an remove Empty

#[derive(Debug, PartialEq)]
pub enum Outcome{
    PlayerOne,
    PlayerTwo,
    Draw,
}

impl Outcome{
    pub fn from_mark(mark: Mark) -> Option<Outcome>{
        match mark{
            Mark::PlayerOne => return Some(Outcome::PlayerOne),
            Mark::PlayerTwo => return Some(Outcome::PlayerTwo),
            _ => None,
        }
    }
}

impl Mark{
    pub fn from_outcome(outcome: Outcome) -> Option<Mark>{
        match outcome{
            Outcome::PlayerOne => return Some(Mark::PlayerOne),
            Outcome::PlayerTwo => return Some(Mark::PlayerTwo),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct Game{
    field: [[Mark; 3]; 3],
}

pub struct PossibleThreeIterator<'a> {
    counter: usize,
    game: &'a Game
}

impl<'a> Iterator for PossibleThreeIterator<'a> {
    type Item = [Mark; 3];

    fn next(&mut self) -> Option<Self::Item> {
        match self.counter{
            0 ... 2 => {
                self.counter += 1;
                return Some( self.game.field[self.counter - 1] );
            },
            3 ... 5 => {
                self.counter += 1;
                let mut transposed = [Mark::Empty;3];
                for j in 0..3{
                    transposed[j] = self.game.field[j][self.counter - 4];
                }
                return Some(transposed);
            }
            6 => {
                    self.counter += 1;
                    let mut diagonal = [Mark::Empty;3];
                    for i in 0..3{
                        diagonal[i] = self.game.field[i][i]
                    }
                    return Some(diagonal)
                 }
            7 => {
                self.counter += 1;
                let mut diagonal = [Mark::Empty;3];
                for i in 0..3{
                    diagonal[i] = self.game.field[i][2-i]
                }
                return Some(diagonal)
            }
            _ => None,
        }
    }
}


impl Game {

    pub fn new() -> Game{
        let array = [[Mark::Empty;3];3];
        Game{ field: array }
    }

    pub fn get_field(&self) -> [[Mark; 3]; 3]{
        self.field
    }

    pub fn get_empty_fields(&self) -> Vec<usize>{
        let mut empty_fields : Vec<(usize,usize)> = Vec::new();
        self.get_field().iter().enumerate().for_each( |(i,row)| {
            empty_fields.append(
                &mut row.iter().enumerate().filter_map( |(j,el)| {
                    match el {
                        Mark::Empty => Some((i,j)),
                        _ => None
                }}).collect::<Vec<(usize,usize)>>()
            );
        });
        empty_fields.iter().map( |index| Game::from_index(*index) ).collect()
    }

    pub fn iter(&self) -> PossibleThreeIterator{
        PossibleThreeIterator{counter: 0 , game: &self}
    }

    pub fn from_index( tuple: (usize,usize) ) -> usize {
        let (i,j) = tuple;
        i * 3 + j
    }

    pub fn to_index( index_num: usize ) -> (usize, usize) {
        (index_num / 3, index_num % 3)
    }

    pub fn evaluate(&self) -> Option<Outcome>{

        for triple in self.iter(){
            if triple[1..].iter().all( |mark| mark == triple.first().unwrap() ){
                match triple.first().unwrap(){
                    Mark::PlayerOne => return Some(Outcome::PlayerOne),
                    Mark::PlayerTwo => return Some(Outcome::PlayerTwo),
                    _ => (),
                }
            }
        }

        if self.iter().all( |triple| !triple.iter().any( |mark| *mark == Mark::Empty ) ){
            Some(Outcome::Draw)
        } else { None }
    }

    pub fn set_mark( &mut self, mark: Mark, position: (usize, usize) ) -> Result<(), String>{
        let (x,y) = position;
        if x < 3 && y < 3 {
            match self.field[x][y]{
                Mark::Empty => {
                    self.field[x][y] = mark;
                    Ok(())
                }
                _ => Err( String::from("Cannot set to nonempty field") ),
            }
        } else {
            Err( String::from( "Wrong position index" ) )
        }
    }
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        write!(f, "{}",
            self.field.into_iter().map( |row| {
                row.into_iter().map( |el| {
                    match el {
                        Mark::PlayerOne => "X",
                        Mark::PlayerTwo => "O",
                        Mark::Empty => "-",
                    }
                }).collect::<Vec<&str>>().join("|")
            }).collect::<Vec<String>>().join("\n")
        )
    }
}

impl fmt::Display for Mark {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        write!(f, "{}",
            match self {
                Mark::PlayerOne => "X",
                Mark::PlayerTwo => "O",
                Mark::Empty => "-",
            }
        )
    }
}

impl From<&Game> for String {
    fn from(game: &Game) -> Self {
        format!("{}", game).chars().filter( |c| *c == 'X' || *c == 'O' || *c == '-'  ).collect()
    }
}
