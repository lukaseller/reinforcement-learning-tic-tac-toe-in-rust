use crate::game::{Game,Mark,Outcome};
use super::traits::Agent;

extern crate rand;
use rand::Rng;

pub struct RandomAgent{
    mark: Mark,
}

impl RandomAgent{
    pub fn new(mark: Mark) -> Self{
        RandomAgent{
            mark: mark,
        }
    }
}

impl Agent for RandomAgent{
    fn next_move(&mut self, game: &Game) -> (usize, usize){
        let empty_fields = game.get_empty_fields();
        let random_index: usize = rand::thread_rng().gen_range(0, empty_fields.len() );
        Game::to_index(empty_fields[random_index])
    }

    fn evaluate_game(&mut self, _outcome: &Outcome){
        ()
    }

    fn get_mark(&self) -> Mark{
        self.mark
    }
}
