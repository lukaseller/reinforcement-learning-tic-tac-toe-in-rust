use crate::game::{Game,Mark,Outcome};

pub trait Agent{
    fn next_move(&mut self, game: &Game) -> (usize, usize);
    fn evaluate_game(&mut self, outcome: &Outcome);
    fn get_mark(&self) -> Mark;
}
