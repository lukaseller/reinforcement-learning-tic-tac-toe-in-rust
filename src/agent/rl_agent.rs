use crate::game::{Game,Mark,Outcome};
use super::traits::Agent;

use std::collections::HashMap;

pub struct RLAgent{
    //String --> field; usize --> action
    //f32 is the rewards
    q_function: HashMap<String, [f32;9]>,
    initial_q_value: f32,
    mark: Mark,
    last_action: Option<(String,usize)>,
    counter: usize,
    alpha: f32,
    discount: f32,
    reward: (f32, f32, f32)
}

impl RLAgent{
    pub fn new(mark: Mark, reward: (f32, f32, f32), initial_q_value: f32) -> Self{
        RLAgent{
            q_function: HashMap::new(),
            mark: mark,
            counter: 0,
            last_action: None,
            alpha: 0.1,
            discount: 0.8,
            reward: reward,
            initial_q_value: initial_q_value
        }
    }
}

impl Agent for RLAgent{

    fn next_move(&mut self, game: &Game) -> (usize, usize){
        let game_string = String::from( game );
        let q_array = {
            match self.q_function.get(&game_string){
               Some(ref q_array) => **q_array,
               None => {
                    self.q_function.insert( game_string.clone() , [self.initial_q_value ;9] );
                    [self.initial_q_value ;9]
               }
            }
        };

        //Value max is used to update the q_function
        let index_max = {
            //From all indexes of the Q-Array, get those which are empty and choose the maximum
            let empty_fields = game.get_empty_fields();
            let possible_indices : Vec<(usize, &f32)> = q_array.iter().enumerate().filter( |(i,_)| {
                    empty_fields.contains(&i)
            }).collect();
            let max = possible_indices.iter().max_by(|(_,a), (_,b)| a.partial_cmp(b).unwrap() ).unwrap();
            possible_indices.iter().find( |(_,el)| *el == max.1 ).unwrap().0
        };

        //Seperate in a learning method?
        if let Some( (game, index) ) = self.last_action.as_ref() {
            //(1 - alpha) * q_array[last_action(String,Index)] + alpha * (Reward + discount * q_array[index_max])

            self.q_function.get_mut(&game[..]).unwrap()[*index] = (1.0-self.alpha) * self.q_function.get_mut(&game[..]).unwrap()[*index]
                + self.alpha * ( 0.0 + self.discount * q_array[index_max]);
        }

        self.last_action = Some((game_string,index_max));
        self.counter += 1;
        Game::to_index(index_max)
    }

    fn evaluate_game(&mut self, outcome: &Outcome) {
        let reward = match outcome{
            Outcome::Draw => self.reward.2,
            outcome if *outcome == Outcome::from_mark(self.mark).unwrap() => self.reward.0,
            _ => self.reward.1
        };

        if let Some( (game, index) ) = self.last_action.as_ref() {
            self.q_function.get_mut(&game[..]).unwrap()[*index] = (1.0-self.alpha) * self.q_function.get_mut(&game[..]).unwrap()[*index]
                + self.alpha * reward;
        } else { panic!("No Previous Action!"); }

        if self.mark == Mark::PlayerOne && self.counter % 10000 == 0 {
            let game = String::from( &Game::new() );
            println!("{:?}", self.q_function.get(&game) );
        }
    }
    fn get_mark(&self) -> Mark{
        self.mark
    }
}
