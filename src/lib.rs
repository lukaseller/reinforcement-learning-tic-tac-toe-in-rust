pub mod game;
pub mod agent;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_to_index_test() {
        for i in 0..8 {
            assert_eq!( game::Game::from_index(game::Game::to_index(i)), i);
        }
        assert_eq!( game::Game::to_index(5), (1,2) );
        assert_eq!( game::Game::to_index(0), (0,0) );
        assert_eq!( game::Game::to_index(8), (2,2) );
    }

}
